package com.walmart.replenishment.odr.framework.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OdrCommonUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OdrCommonUiApplication.class, args);
	}
}
