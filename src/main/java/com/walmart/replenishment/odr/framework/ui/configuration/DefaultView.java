package com.walmart.replenishment.odr.framework.ui.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class DefaultView extends WebMvcConfigurerAdapter {
	
	@Override
	public void addViewControllers(ViewControllerRegistry registry ) {
		registry.addViewController("/").setViewName("forward:/index.html");
		registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
		super.addViewControllers(registry);
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry){
		registry.addResourceHandler("/**/*.*","/*.*").addResourceLocations("/RootApp/");
//		This lets access all the files in the RootApp directory. 
//		if(!registry.hasMappingForPattern("/RootApp/**")) {
//			registry.addResourceHandler("/RootApp/**").addResourceLocations("/RootApp/");
//		}
	}

}
