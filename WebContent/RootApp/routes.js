odrApp.config(function($stateProvider, $urlRouterProvider){
    // Got lost always take the user to main page.
    $urlRouterProvider.otherwise('/home');

    $stateProvider.state('home', {
        url:'/home',
        templateUrl: 'apps/landingpage/views/home-page.html'

    });//End of state provider.
});


// Shift this to a seperate file for theming.

// Available palettes: red, pink, purple, deep-purple, indigo, blue, light-blue, cyan, teal, green, light-green, lime, yellow, amber, orange, deep-orange, brown, grey, blue-grey
odrApp.config(function($mdThemingProvider){
    // Try dark theming
    $mdThemingProvider.theme('default').primaryPalette('blue').accentPalette('cyan').dark();
});

